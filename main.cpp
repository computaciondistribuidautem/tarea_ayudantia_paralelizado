#include <iostream>
#include <fstream> //Librería para ocupar fstream
#include <sstream> //Libreria para ocupar std::istringstream
#include <string> // Librería string
#include <vector> // Librería std::vector
#include <math.h> // Librería matemática sqrt(raices) y acos
#include <omp.h> // libreria para el paralelismo
#include <chrono> // libreria para el tiempo

using namespace std;

#define PI 3.14159265
std::vector<string> arreglo;
float x1,x2,x3,auxy1,auxy2,auxy3,anguloA,anguloB,anguloC;
string verticeA,verticeB,verticeC;
int contador3,contador4,nuevocontador=0,cpu,tiempo;

void evaluartriangulo(string A,string B,string C, float angulo)
{
    ofstream salida("nuevoespiral.mesh", ofstream::app);
    ofstream salidanodos("nuevoespiral.node",ofstream::app);
    float distancia1,distancia2,distancia3,nuevadistancia1,nuevacoordenadaX,nuevacoordenadaY;
    #pragma omp parallel num_threads(cpu)
    {
        #pragma omp for
        for(int j=0;j<arreglo.size();j++)
        {
            string aux,aux2,aux3,aux4=arreglo[j];
            std::istringstream iss(aux4);
            iss>>aux>>aux2>>aux3;
            if(A==aux)
            {
                verticeA=aux;
                x1=std::stof(aux2);
                auxy1=std::stof(aux3);
                //cout<<verticeA<<" "<<x1<<" "<<auxy1<<endl;
            }
            if(B==aux)
            {
                verticeB=aux;
                x3=std::stof(aux2);
                auxy3=std::stof(aux3);
                //cout<<verticeB<<" "<<x3<<" "<<auxy3<<endl;
            }
            if(C==aux)
            {
                verticeC=aux;
                x2=std::stof(aux2);
                auxy2=std::stof(aux3);
                //cout<<verticeC<<" "<<x2<<" "<<auxy2<<endl;
            }
        }
    }
    /*cout<<"El punto "<<verticeA<<" con coordenadas en x: "<<x1<<" y con coordenadas en y: "<<auxy1<<endl;
    cout<<"El punto "<<verticeB<<" con coordenadas en x: "<<x2<<" y con coordenadas en y: "<<auxy2<<endl;
    cout<<"El punto "<<verticeC<<" con coordenadas en x: "<<x3<<" y con coordenadas en y: "<<auxy3<<endl;*/
    distancia1=sqrt(pow((x2 - x1),2) + pow((auxy2-auxy1),2));// distancia entre los puntos A y B
    distancia2=sqrt(pow((x3 - x1),2) + pow((auxy3 - auxy1),2));// distancia entre los puntos A y C
    distancia3=sqrt(pow((x3 - x2),2) + pow((auxy3 - auxy2),2));// distancia entre los puntos B y C
    /*cout<<"Distancia entre los puntos A y B es de: "<<distancia1<<endl;
    cout<<"Distancia entre los puntos A y C es de: "<<distancia2<<endl;
    cout<<"Distancia entre los puntos B y C es de: "<<distancia3<<endl;*/
    anguloA = acos((distancia2*distancia2+distancia3*distancia3-distancia1*distancia1)/(2*distancia2*distancia3))*180/PI;
    anguloB = acos((distancia1*distancia1+distancia3*distancia3-distancia2*distancia2)/(2*distancia1*distancia3))*180/PI;
    anguloC = acos((distancia1*distancia1+distancia2*distancia2-distancia3*distancia3)/(2*distancia1*distancia2))*180/PI;
    /*cout<<"Angulo A: "<<anguloA<<endl;
    cout<<"Angulo B: "<<anguloB<<endl;
    cout<<"Angulo C: "<<anguloC<<endl;*/
    if(angulo<=anguloA || angulo<=anguloB || angulo<=anguloC)
    {
       // cout<<"SI"<<endl;
        contador3++;
        if(distancia1>distancia2 && distancia1>distancia3)
        {
            nuevadistancia1=distancia1/2;// nuevo punto D formando el triangulo ACD y BCD
            nuevacoordenadaX=(x1+nuevadistancia1*x2)/(1+nuevadistancia1);
            nuevacoordenadaY=(auxy1+nuevadistancia1*auxy2)/(1+nuevadistancia1);
            salida<<verticeA<<" "<<verticeC<<" D"<<contador3<<endl;
            salida<<verticeB<<" "<<verticeC<<" D"<<contador3<<endl;
            salidanodos<<"D"<<contador3<<" "<<nuevacoordenadaX<<" "<<nuevacoordenadaY<<endl; //IMPRIMIENDO EL NUEVO VERTICE CON SU COORDENADA EN EL ARCHIVO NODE
            //cout<<"A coordenada del punto #"<<contador3<<" en el eje X: "<<nuevacoordenadaX<<" -  en el eje Y: "<<nuevacoordenadaY<<endl;
            //distancia A hasta C es la misma
            //distancia B hasta C es la misma
        }
        else
        {
            if(distancia2>distancia3)
            {
                nuevadistancia1=distancia2/2;// nuevo punto D formando el triangulo BAD y CAD
                nuevacoordenadaX=(x1+nuevadistancia1*x3)/(1+nuevadistancia1);
                nuevacoordenadaY=(auxy1+nuevadistancia1*auxy3)/(1+nuevadistancia1);
                salida<<verticeA<<" "<<verticeC<<" D"<<contador3<<endl;
                salida<<verticeA<<" "<<verticeB<<" D"<<contador3<<endl;
                salidanodos<<"D"<<contador3<<" "<<nuevacoordenadaX<<" "<<nuevacoordenadaY<<endl; //IMPRIMIENDO EL NUEVO VERTICE CON SU COORDENADA EN EL ARCHIVO NODE
                //cout<<"B coordenada del punto #"<<contador3<<" en el eje X: "<<nuevacoordenadaX<<" -  en el eje Y: "<<nuevacoordenadaY<<endl;
            }
            else
            {
                nuevadistancia1=distancia3/2;// nuevo punto D formando el triangulo CBD y ABD
                nuevacoordenadaX=(x2+nuevadistancia1*x3)/(1+nuevadistancia1);
                nuevacoordenadaY=(auxy2+nuevadistancia1*auxy3)/(1+nuevadistancia1);
                salida<<verticeA<<" "<<verticeB<<" D"<<contador3<<endl;
                salida<<verticeB<<" "<<verticeC<<" D"<<contador3<<endl;
                salidanodos<<"D"<<contador3<<" "<<nuevacoordenadaX<<" "<<nuevacoordenadaY<<endl; //IMPRIMIENDO EL NUEVO VERTICE CON SU COORDENADA EN EL ARCHIVO NODE
                //cout<<"C coordenada del punto #"<<contador3<<" en el eje X: "<<nuevacoordenadaX<<" -  en el eje Y: "<<nuevacoordenadaY<<endl;
            }
        }
    }
    else
    {
        contador4++;
        salida<<verticeA<<" "<<verticeB<<" "<<verticeC<<endl; // IMPRIMIENDO LOS VERTICES DEL TRIANGULO EN EL NUEVO ARCHIVO MESH
        /*salidanodos<<verticeA<<" "<<x1<<" "<<auxy1<<endl; //IMPRIMIENDO EL VERTICE A EN EL NUEVO ARCHIVO NODE, CON SUS COORDENADAS
        salidanodos<<verticeB<<" "<<x3<<" "<<auxy3<<endl; //IMPRIMIENDO EL VERTICE B EN EL NUEVO ARCHIVO NODE, CON SUS COORDENADAS
        salidanodos<<verticeC<<" "<<x2<<" "<<auxy2<<endl; //IMPRIMIENDO EL VERTICE C EN EL NUEVO ARCHIVO NODE, CON SUS COORDENADAS*/
    }
    salida.close();
    salidanodos.close();
}

int main()
{
    int contador=0,contador2=0;
    string puntoA,puntoB,puntoC,num,x, y, line;
    float angulo;
    cout<<"Ingresar angulo:"<<endl;
    cin>>angulo;
    cout<<"Ingrese cantidad de hilos a ocupar"<<endl;
    cin>>cpu;
    ifstream nodos("espiral.node", ifstream::in);//se abre el archivo en modo lectura
    ifstream malla("espiral.mesh",ifstream::in);
    ofstream salidanodos("nuevoespiral.node",ofstream::app);
    std::string str;
    std::getline(malla, str); //obtiene la primera linea que es innecesaria
    auto start=chrono::steady_clock::now();
    do
    {
        contador++;
        nodos>>num>>x>>y;
        arreglo.push_back(num+" "+x+" "+y);
        salidanodos<<num<<" "<<x<<" "<<y<<endl;
        //cout<<contador<<" - "<<numero<<" - "<<coordenadax<<" - "<<coordenaday<<endl;
    }
    while(!nodos.eof());
    salidanodos.close();
    nodos.close();
    do
    {
        contador2++;
        malla>>puntoA>>puntoB>>puntoC;
        //cout<<puntoA<<" - "<<puntoB<<" - "<<puntoC<<endl;
        evaluartriangulo(puntoA,puntoB,puntoC, angulo);
        //break;
    }
    while(!malla.eof());
    auto end=chrono::steady_clock::now();
    malla.close();
    cout<<"Se han refinado "<<contador3<<" en total"<<endl;
    tiempo=chrono::duration_cast<chrono::seconds>(end - start).count();
    cout<<"Con un tiempo de: "<<tiempo<<" segundos en total"<<endl;
    //cout<<"Hay "<<contador3<<" triangulos que refinar y "<<contador4<<" triangulos listos para guardar"<<endl;
}
